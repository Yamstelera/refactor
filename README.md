### Refactor test task

Run application code:
1) `composer install`
2) `php app.php input.txt`

Run application tests:
1) `composer install`
2) `vendor/bin/phpunit`
