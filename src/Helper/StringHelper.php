<?php

namespace App\Helper;

/**
 * Class StringHelper
 */
class StringHelper
{
    /**
     * Check is string is jason formatted.
     *
     * @param $string
     *
     * @return bool
     */
    public static function isJson($string): bool
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Check European country code.
     *
     * @param string $countryCode
     *
     * @return bool
     */
    public static function isEurope(string $countryCode): bool
    {
        /**
         * List of European country codes.
         */
        $europeCountryCodes = [
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU',
            'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK',
        ];

        return in_array($countryCode, $europeCountryCodes);
    }
}
