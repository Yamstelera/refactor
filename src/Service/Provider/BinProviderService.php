<?php

namespace App\Service\Provider;

use App\Exception\EmptyDataException;

/**
 * Class BinProviderService
 *
 * @package App\Service
 */
class BinProviderService extends BaseProvider implements ProvidableInterface
{
    private const PROVIDER_BIN_LIST  = 'https://lookup.binlist.net/';

    /**
     * {@inheritdoc}
     */
    public function run(array $data): array
    {
        $result = [];

        foreach ($data as $bin) {
            if (!isset($result[$bin])) {
                $singleBinResult = $this->jsonFileReader->getFileRows(self::PROVIDER_BIN_LIST . $bin);

                if (empty($singleBinResult)) {
                    throw new EmptyDataException('Bin data is not provide');
                }

                $result[$bin] = $singleBinResult['country']['alpha2'] ?? null;
            }
        }

        return $result;
    }
}
