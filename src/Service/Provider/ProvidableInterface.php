<?php

namespace App\Service\Provider;

use App\Exception\EmptyDataException;

/**
 * Interface ProvidableInterface
 *
 * @package App\Service\Provider
 */
interface ProvidableInterface
{
    /**
     * Fetch data provider.
     *
     * @param array $data
     *
     * @return array
     *
     * @throws EmptyDataException
     */
    public function run(array $data): array;
}
