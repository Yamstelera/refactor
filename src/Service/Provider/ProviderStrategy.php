<?php

namespace App\Service\Provider;

/**
 * Class ProviderStrategy
 *
 * @package App\Service\Provider
 */
class ProviderStrategy
{
    /**
     * @var ProvidableInterface
     */
    protected $provider;

    /**
     * BaseProvider constructor.
     *
     * @param ProvidableInterface $provider
     */
    public function __construct(ProvidableInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * {@inheritdoc}
     */
    public function run(array $data): array
    {
        return $this->provider->run($data);
    }
}