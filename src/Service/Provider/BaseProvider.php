<?php

namespace App\Service\Provider;

use App\Service\FileReader\FileReaderStrategy;
use App\Service\FileReader\JsonFileReader;
use App\Service\FileReader\TransactionFileReader;

/**
 * Class BaseProvider
 *
 * @package App\Service\Provider
 */
class BaseProvider
{
    /**
     * @var JsonFileReader
     */
    protected $jsonFileReader;

    /**
     * @var TransactionFileReader
     */
    protected $transactionFileReader;

    /**
     * BinProviderService constructor.
     */
    public function __construct()
    {
        $this->transactionFileReader = new FileReaderStrategy(new TransactionFileReader());
        $this->jsonFileReader        = new FileReaderStrategy(new JsonFileReader());
    }
}
