<?php

namespace App\Service\Provider;

use App\Exception\EmptyDataException;

/**
 * Class ExchangeProviderService
 *
 * @package App\Service
 */
class ExchangeProviderService extends BaseProvider implements ProvidableInterface
{
    private const PROVIDER_EXCHANGE  = 'https://api.exchangeratesapi.io/latest';

    /**
     * {@inheritdoc}
     */
    public function run(array $data): array
    {
        $result = [];

        $providerData = $this->jsonFileReader->getFileRows(self::PROVIDER_EXCHANGE);
        if (empty($providerData)) {
            throw new EmptyDataException('Exchange data is not provide');
        }

        foreach ($data as $currency) {
            if (!isset($result[$currency])) {
                $result[$currency] = $providerData['rates'][$currency] ?? null;
            }
        }

        return $result;
    }
}
