<?php

namespace App\Service\FileReader;

/**
 * Service to process JSON file resources.
 *
 * @package App\Service
 */
class JsonFileReader extends FileReader implements FileReadableInterface
{
    /**
     * {@inheritdoc}
     */
    public function getFileRows(string $filename): array
    {
        $rows = [];

        if ($filename) {
            $fileContent = file_get_contents($filename);

            if ($fileContent) {
                $rows = json_decode($fileContent, true) ?? [];
            }
        }

        return $rows;
    }
}
