<?php

namespace App\Service\FileReader;

use App\Exception\FileNotFoundException;

/**
 * Interface FileReadableInterface
 *
 * @package App\Service
 */
interface FileReadableInterface
{
    /**
     * Return file rows.
     *
     * @param string $filename
     *
     * @return array
     */
    public function getFileRows(string $filename): array;

    /**
     * Base validate file for existing.
     *
     * @param string|null $processFile
     *
     * @return bool
     *
     * @throws FileNotFoundException
     */
    public function fileValid(string $processFile = null): bool;
}
