<?php

namespace App\Service\FileReader;

use App\Exception\FileNotFoundException;

/**
 * Service to process file resources.
 *
 * @package App\Service
 */
class FileReader
{
    /**
     * {@inheritdoc}
     */
    public function fileValid(string $processFile = null): bool
    {
        $result = true;

        if (!file_exists($processFile)) {
            throw new FileNotFoundException('File not found');
        }

        if (!($file = fopen($processFile, 'r'))) {
            throw new FileNotFoundException('Failed to open file');
        }

        return $result;
    }
}
