<?php

namespace App\Service\FileReader;

/**
 * Class FileReaderStrategy
 *
 * @package App\Service\Provider
 */
class FileReaderStrategy
{
    /**
     * @var FileReadableInterface
     */
    protected $fileReader;

    /**
     * FileReaderStrategy constructor.
     *
     * @param FileReadableInterface $fileReader
     */
    public function __construct(FileReadableInterface $fileReader)
    {
        $this->fileReader = $fileReader;
    }

    /**
     * {@inheritdoc}
     */
    public function getFileRows(string $filename): array
    {
        return $this->fileReader->getFileRows($filename);
    }

    /**
     * {@inheritdoc}
     */
    public function fileValid(string $processFile = null): bool
    {
        return $this->fileReader->fileValid($processFile);
    }
}