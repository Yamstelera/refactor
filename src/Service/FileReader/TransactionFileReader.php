<?php

namespace App\Service\FileReader;

/**
 * Service to process file with transactions resources.
 *
 * @package App\Service
 */
class TransactionFileReader extends FileReader implements FileReadableInterface
{
    /**
     * {@inheritdoc}
     */
    public function getFileRows(string $filename): array
    {
        $rows = [];

        if ($filename) {
            $fileContent = file_get_contents($filename);

            if ($fileContent) {
                $data = explode("\n", file_get_contents($filename));

                $rows = array_map(function ($row) {
                    return json_decode($row, true);
                }, $data);
            }
        }

        return $rows;
    }
}
