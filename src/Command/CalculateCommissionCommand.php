<?php

namespace App\Command;

use App\Exception\FileNotFoundException;
use App\Helper\StringHelper;
use App\Service\FileReader\FileReadableInterface;
use App\Service\Provider\BinProviderService;
use App\Service\Provider\ExchangeProviderService;
use App\Service\FileReader\JsonFileReader;
use App\Service\FileReader\TransactionFileReader;
use App\Service\FileReader\FileReaderStrategy;
use App\Service\Provider\ProvidableInterface;
use App\Service\Provider\ProviderStrategy;

/**
 * Class CalculateCommissionCommand
 *
 * @package App\Command
 */
class CalculateCommissionCommand extends BaseCommand
{
    public const COMMISSION_EUR          = 0.01;
    public const COMMISSION_NOT_EUR      = 0.02;
    public const PROCESS_FILE_DIR_PREFIX = __DIR__ . '/../../';

    /**
     * @var FileReadableInterface
     */
    private $transactionFileReader;

    /**
     * @var FileReadableInterface
     */
    private $jsonFileReader;

    /**
     * @var ProvidableInterface
     */
    private $binProviderService;

    /**
     * @var ProvidableInterface
     */
    private $exchangeProviderService;

    /**
     * List of bin data provider.
     *
     * @var array
     */
    private $binResults = [];

    /**
     * List of rate data provider.
     *
     * @var array
     */
    private $rateResults = [];

    /**
     * CalculateCommissionCommand constructor.
     */
    public function __construct()
    {
        $this->transactionFileReader   = new FileReaderStrategy(new TransactionFileReader());
        $this->jsonFileReader          = new FileReaderStrategy(new JsonFileReader());
        $this->binProviderService      = new ProviderStrategy(new BinProviderService());
        $this->exchangeProviderService = new ProviderStrategy(new ExchangeProviderService());
    }

    /**
     * Return list of commissions result.
     *
     * @param array $arguments
     *
     * @return array
     */
    public function run(array $arguments = [])
    {
        $result = [];

        try {
            $processFile = $this->getProcessFile($arguments);
            $processFile = self::PROCESS_FILE_DIR_PREFIX . $processFile;

            if ($this->transactionFileReader->fileValid($processFile)) {
                $fileRows = $this->transactionFileReader->getFileRows($processFile);

                $this->binResults  = $this->binProviderService->run(array_column($fileRows, 'bin'));
                $this->rateResults = $this->exchangeProviderService->run(array_column($fileRows, 'currency'));

                foreach ($fileRows as $row) {
                    array_push($result, $this->processTransaction($row));
                }
            }
        } catch (\Exception $e) {
            $result = $this->exception($e);
        }

        return $result;
    }

    /**
     * Process single transaction row.
     *
     * @param array $row
     *
     * @return float|int|null
     */
    private function processTransaction(array $row)
    {
        $amountFixed = $this->calculateAmountFix($row);

        return $amountFixed
            ? $amountFixed * $this->getCommissionValue($row['bin'])
            : 1;
    }

    /**
     * Calculate amount fix value.
     *
     * @param $data
     *
     * @return float|int|null
     */
    private function calculateAmountFix($data)
    {
        $amountFixed = null;
        $rateResult  = $this->rateResults[$data['currency']] ?? null;

        if ($data['currency'] == 'EUR' || $rateResult == 0) {
            $amountFixed = $data['amount'];
        }

        if ($data['currency'] != 'EUR' || $rateResult > 0) {
            $amountFixed = $data['amount'] / $rateResult;
        }

        return ceil($amountFixed);
    }

    /**
     * Apply different commission rates for EU-issued and non-EU-issued cards.
     *
     * @param int $bin
     *
     * @return float
     */
    private function getCommissionValue(int $bin)
    {
        $countryCode = $this->binResults[$bin] ?? '';

        $isEu = StringHelper::isEurope($countryCode);

        return $isEu
            ? self::COMMISSION_EUR
            : self::COMMISSION_NOT_EUR;
    }

    /**
     * Return transaction file name parameter.
     *
     * @param array $arguments
     *
     * @return null|string
     *
     * @throws FileNotFoundException
     */
    private function getProcessFile(array $arguments = [])
    {
        if (!empty($arguments) && isset($arguments[1])) {
            $processFile = $arguments[1];
        } else {
            throw new FileNotFoundException('Transaction file data not found');
        }

        return $processFile;
    }
}
