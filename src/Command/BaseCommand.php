<?php

namespace App\Command;

/**
 * Class BaseCommand
 *
 * @package App\Command
 */
class BaseCommand
{
    /**
     * @param \Exception $e
     *
     * @return string
     */
    public function exception(\Exception $e)
    {
        return sprintf('%s: %s', get_class($e), $e->getMessage()). "\n";
    }

    /**
     * @param array $data
     */
    public function stat(array $data = [])
    {
        if ($data) {
            foreach ($data as $rowData) {
                echo $rowData . "\n";
            }
        } else {
            echo 'Data is empty';
        }
    }
}
