<?php

namespace App\Exception;

/**
 * Class FileNotFoundException
 */
class FileNotFoundException extends \Exception
{
}
