<?php

namespace App\Exception;

/**
 * Class EmptyDataException
 *
 * @package App\Exception
 */
class EmptyDataException extends \Exception
{
}
