<?php

use App\Command\CalculateCommissionCommand;
use App\Exception\FileNotFoundException;
use App\Service\FileReader\FileReadableInterface;
use App\Service\FileReader\FileReaderStrategy;
use App\Service\FileReader\TransactionFileReader;
use App\Service\Provider\BinProviderService;
use App\Service\Provider\ExchangeProviderService;
use App\Service\Provider\ProvidableInterface;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Test\PHPUnitUtil;

/**
 * Class CalculateCommissionCommandTest
 */
class CalculateCommissionCommandTest extends MockeryTestCase
{
    /**
     * @var array
     */
    protected $binResult = [
        45717360 => 'DK',
        516793   => 'LT',
        45417360 => 'JP',
        41417360 => 'US',
        4745030  => 'GB',
    ];

    /**
     * @var
     */
    protected $exchangeResult;

    /**
     * @var array
     */
    protected $transactions;

    /**
     * CalculateCommissionCommandTest constructor.
     *
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->exchangeResult = json_decode(
            file_get_contents(__DIR__ . '/../exchange-provider-fixture.json'), true
        )['rates'];

        $this->transactions = (new TransactionFileReader())->getFileRows(__DIR__ . '/../../input.txt');
    }

    public function testCalculateCommissionRunning()
    {
        $command = new CalculateCommissionCommand();

        $transactionFileReaderMock = Mockery::mock(TransactionFileReader::class);
        PHPUnitUtil::setPrivateProperty($command, 'transactionFileReader', $transactionFileReaderMock);
        $transactionFileReaderMock
            ->shouldReceive('fileValid')
            ->once()
            ->with(CalculateCommissionCommand::PROCESS_FILE_DIR_PREFIX . 'input.txt')
            ->andReturn(true);
        $transactionFileReaderMock
            ->shouldReceive('getFileRows')
            ->once()
            ->with(CalculateCommissionCommand::PROCESS_FILE_DIR_PREFIX . 'input.txt')
            ->andReturn($this->transactions);

        $binProviderServiceMock = Mockery::mock(BinProviderService::class);
        PHPUnitUtil::setPrivateProperty($command, 'binProviderService', $binProviderServiceMock);
        $binProviderServiceMock
            ->shouldReceive('run')
            ->once()
            ->with(array_column($this->transactions, 'bin'))
            ->andReturn($this->binResult);

        $exchangeProviderServiceMock = Mockery::mock(ExchangeProviderService::class);
        PHPUnitUtil::setPrivateProperty($command, 'exchangeProviderService', $exchangeProviderServiceMock);
        $exchangeProviderServiceMock
            ->shouldReceive('run')
            ->once()
            ->with(array_column($this->transactions, 'currency'))
            ->andReturn($this->exchangeResult);

        $commissions = $command->run(['1' => 'input.txt']);

        $this->assertTrue($transactionFileReaderMock instanceof FileReadableInterface);
        $this->assertTrue($binProviderServiceMock instanceof ProvidableInterface);
        $this->assertTrue($exchangeProviderServiceMock instanceof ProvidableInterface);
        $this->assertEquals(count($commissions), count($this->transactions));

        // For EUR currency for test case order to transaction file example.
        $this->assertEquals($commissions[0], 1);
    }

    public function testCalculateCommissionRunningFileInputEmptyException()
    {
        $command = new CalculateCommissionCommand();

        $exceptionTexts = $command->exception(new FileNotFoundException('Transaction file data not found'));
        $commandResult = $command->run([]);

        $this->assertEquals($exceptionTexts, $commandResult);
    }

    public function testGetCommissionValue()
    {
        $command = new CalculateCommissionCommand();

        PHPUnitUtil::setPrivateProperty($command, 'binResults', $this->binResult);

        $commissionValueDK = PHPUnitUtil::getPrivateMethod($command, 'getCommissionValue', [45717360]);
        $this->assertEquals($commissionValueDK, CalculateCommissionCommand::COMMISSION_EUR);

        $commissionValueAny = PHPUnitUtil::getPrivateMethod($command, 'getCommissionValue', [rand(0, 10)]);
        $this->assertEquals($commissionValueAny, CalculateCommissionCommand::COMMISSION_NOT_EUR);
    }

    public function testCalculateAmountFixForEur()
    {
        $singleTransaction = $this->transactions[0];

        $command = new CalculateCommissionCommand();

        PHPUnitUtil::setPrivateProperty($command, 'rateResults', $this->exchangeResult);

        $mountFix = PHPUnitUtil::getPrivateMethod($command, 'calculateAmountFix', [$singleTransaction]);

        $this->assertEquals($mountFix, ceil($singleTransaction['amount']));
    }

    public function testCalculateAmountFixForOtherCurrency()
    {
        $singleTransaction = $this->transactions[2];

        $command = new CalculateCommissionCommand();

        PHPUnitUtil::setPrivateProperty($command, 'rateResults', $this->exchangeResult);

        $mountFix = PHPUnitUtil::getPrivateMethod($command, 'calculateAmountFix', [$singleTransaction]);

        $rateResult = $this->exchangeResult[$singleTransaction['currency']];

        $this->assertEquals($mountFix, ceil($singleTransaction['amount']/$rateResult));
    }
}
