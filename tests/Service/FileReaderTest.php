<?php

use App\Exception\FileNotFoundException;
use App\Service\FileReader\FileReader;
use App\Service\FileReader\JsonFileReader;
use App\Service\FileReader\TransactionFileReader;
use PHPUnit\Framework\TestCase;

/**
 * Class FileReaderTest
 */
class FileReaderTest extends TestCase
{
    /**
     * @var JsonFileReader
     */
    protected $jsonFileReader;

    /**
     * @var TransactionFileReader;
     */
    protected $fileTransactionReader;

    /**
     * FileReaderTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->jsonFileReader        = new JsonFileReader();
        $this->fileTransactionReader = new TransactionFileReader();
    }

    public function testFileValidationException()
    {
        $fileReader = new FileReader();

        $this->expectException(FileNotFoundException::class);

        $fileReader->fileValid(null);
    }

    public function testEmptyFileNameJsonFileReader()
    {
        $this->assertEmpty($this->jsonFileReader->getFileRows(''));
    }

    public function testNotCorrectJsonFileJsonFileReader()
    {
        $this->assertEmpty($this->jsonFileReader->getFileRows(__DIR__ . '/../../input.txt'));
    }

    public function testCorrectJsonFileJsonFileReader()
    {
        $row = $this->jsonFileReader->getFileRows(__DIR__ . '/../bin-provider-fixture.json');

        $this->assertTrue(is_array($row));
    }

    public function testEmptyFileNameTransactionFileReader()
    {
        $this->assertEmpty($this->fileTransactionReader->getFileRows(''));
    }

    public function testCorrectTransactionFileJsonFileReader()
    {
        $rows = $this->fileTransactionReader->getFileRows(__DIR__ . '/../../input.txt');

        $this->assertEquals(5, count($rows));
    }
}
