<?php

use App\Exception\EmptyDataException;
use App\Service\FileReader\JsonFileReader;
use App\Service\Provider\BinProviderService;
use App\Service\Provider\ExchangeProviderService;
use PHPUnit\Framework\TestCase;
use Test\PHPUnitUtil;

/**
 * Class ProviderTest
 */
class ProviderTest extends TestCase
{
    public function testExchangeProviderServiceRunning()
    {
        $currency = 'USD';
        $providerData = json_decode(file_get_contents(__DIR__ . '/../exchange-provider-fixture.json'), true);

        $provider = new ExchangeProviderService();

        $jsonFileReaderMock = Mockery::mock(JsonFileReader::class);
        PHPUnitUtil::setPrivateProperty($provider, 'jsonFileReader', $jsonFileReaderMock);
        $jsonFileReaderMock
            ->shouldReceive('getFileRows')
            ->once()
            ->andReturn($providerData);

        $result = $provider->run([$currency]);

        $this->assertEquals($result, [$currency => 1.121]);
    }

    public function testExchangeProviderServiceRunningWithException()
    {
        $provider = new ExchangeProviderService();

        $jsonFileReaderMock = Mockery::mock(JsonFileReader::class);
        PHPUnitUtil::setPrivateProperty($provider, 'jsonFileReader', $jsonFileReaderMock);
        $jsonFileReaderMock
            ->shouldReceive('getFileRows')
            ->once()
            ->andReturn([]);

        $this->expectException(EmptyDataException::class);

        $provider->run(['USD']);
    }

    public function testBinProviderServiceRunning()
    {
        $bin = 516793;
        $binData = json_decode(file_get_contents(__DIR__ . '/../bin-provider-fixture.json'), true);
        $provider = new BinProviderService();

        $jsonFileReader = $this->createMock(JsonFileReader::class);
        PHPUnitUtil::setPrivateProperty($provider, 'jsonFileReader', $jsonFileReader);
        $jsonFileReader
            ->expects($this->once())
            ->method('getFileRows')
            ->willReturn($binData);

        $result = $provider->run([$bin]);

        $this->assertTrue(array_key_exists($bin, $result));
    }

    public function testBinProviderServiceRunningWithException()
    {
        $bin = 516793;
        $provider = new BinProviderService();

        $jsonFileReader = $this->createMock(JsonFileReader::class);
        PHPUnitUtil::setPrivateProperty($provider, 'jsonFileReader', $jsonFileReader);
        $jsonFileReader
            ->expects($this->once())
            ->method('getFileRows')
            ->willReturn([]);

        $this->expectException(EmptyDataException::class);

        $provider->run([$bin]);
    }
}
