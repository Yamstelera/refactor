<?php

namespace Test;

use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

/**
 * Class PHPUnitUtil
 *
 * @package Tests
 */
class PHPUnitUtil
{
    /**
     * Get a private or protected method..
     *
     * @param object $obj
     * @param string $methodName
     * @param array  $args
     *
     * @return ReflectionMethod
     */
    public static function getPrivateMethod($obj, string $methodName, array $args)
    {
        $class = new ReflectionClass($obj);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($obj, $args);
    }

    /**
     * Get a private or protected method..
     *
     * @param object $obj
     * @param string $propertyName
     * @param mixed  $value
     *
     * @return ReflectionProperty
     */
    public static function setPrivateProperty($obj, string $propertyName, $value = null)
    {
        $class = new ReflectionClass($obj);
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);
        if ($value) {
            $property->setValue($obj, $value);
        }

        return $property;
    }
}
