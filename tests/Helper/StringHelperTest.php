<?php

use App\Helper\StringHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class StringHelperTest
 */
class StringHelperTest extends TestCase
{
    public function testIsJson()
    {
        $validJson = file_get_contents(__DIR__ . '/../exchange-provider-fixture.json');

        $this->assertFalse(StringHelper::isJson(''));
        $this->assertTrue(StringHelper::isJson($validJson));
    }

    public function testIsEurope()
    {
        $this->assertTrue(StringHelper::isEurope('LV'));
        $this->assertFalse(StringHelper::isEurope('USA'));
    }
}
