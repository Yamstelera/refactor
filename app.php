<?php

require __DIR__ . '/vendor/autoload.php';

$calculateCommission = new App\Command\CalculateCommissionCommand;

$commissions = $calculateCommission->run($argv);
$calculateCommission->stat($commissions);